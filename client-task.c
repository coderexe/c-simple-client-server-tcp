#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>

#include <arpa/inet.h>
#include <unistd.h>

#define MAX 80
#define PORT 8080
#define SA struct sockaddr
void chat(int sockfd)
{
    char buff[MAX];
    int n;
    while(1) {
        bzero(buff, sizeof(buff));
        printf("Enter: ");
        n = 0;
        while ((buff[n++] = getchar()) != '\n')
            ;
        send(sockfd, buff, sizeof(buff), 0);
        bzero(buff, sizeof(buff));
        recv(sockfd, buff, sizeof(buff), 0);
        printf("From Server : %s", buff);
        if ((strncmp(buff, "Exit", 4)) == 0) {
            printf("Client Exit\n");
            break;
        }
    }
}
   
int main()
{
    int sockfd, connfd;
    struct sockaddr_in servaddr, cli;
   
    // socket create and verification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) {
        printf("socket creation failed\n");
        exit(0);
    }
    else
        printf("Socket creation succesful\n");
    bzero(&servaddr, sizeof(servaddr));
   
    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    servaddr.sin_port = htons(PORT);
   
    // connect the client socket to server socket
    if (connect(sockfd, (SA*)&servaddr, sizeof(servaddr)) != 0) {
        printf("Connection failed\n");
        exit(0);
    }
    else
        printf("Connected\n");
   
    chat(sockfd);
   
    close(sockfd);
}
