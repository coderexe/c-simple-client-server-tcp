#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <unistd.h>

#define MAX 80
#define PORT 8080
#define SA struct sockaddr
   

char *strrev(char *str)
{
      char *p1, *p2;

      if (! str || ! *str)
            return str;
      for (p1 = str, p2 = str + strlen(str) - 2; p2 > p1; ++p1, --p2)
      {
            *p1 ^= *p2;
            *p2 ^= *p1;
            *p1 ^= *p2;
      }
      return str;
}


void chat(int connfd)
{
    char buff[MAX];
    int n;
    while(1) {
        bzero(buff, MAX);
   
        recv(connfd, buff, sizeof(buff), 0);
        printf("From client: %s\n", buff);

        if (strncmp("Exit", buff, 4) == 0)
	{
            printf("Terminating connection\n");
            break;
	}

	strrev(buff);
   	printf("To client: %s\n", buff);

        send(connfd, buff, sizeof(buff), 0);
   
        // if msg contains "Exit" then server exit and chat ended.

    }
}
   
// Driver function
int main()
{
    int sockfd, connfd, len;
    struct sockaddr_in servaddr, cli;
   
    // socket create and verification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) {
        printf("socket creation failed\n");
        exit(0);
    }
    else
        printf("Socket successfully created\n");
    bzero(&servaddr, sizeof(servaddr));
   
    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(PORT);
   
    // Binding newly created socket to given IP and verification
    if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0) {
        printf("Socket bind failed\n");
        exit(0);
    }
    else
        printf("Socket successfully binded\n");
   
    // Now server is ready to listen and verification
    if ((listen(sockfd, 5)) != 0) {
        printf("Listen failed\n");
        exit(0);
    }
    else
        printf("Server listening\n");
    len = sizeof(cli);
   
    // Accept the data packet from client and verification
    connfd = accept(sockfd, (SA*)&cli, &len);
    if (connfd < 0) {
        printf("Server accept failed\n");
        exit(0);
    }
    else
        printf("Server accept the client\n");
   
    // Function for chatting between client and server
    chat(connfd);
   
    // After chatting close the socket
    close(sockfd);
}
